<?php
/**
 * ACG functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ACG
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'acg_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function acg_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ACG, use a find and replace
		 * to change 'acg' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'acg', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'acg' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'acg_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'acg_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function acg_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'acg_content_width', 640 );
}
add_action( 'after_setup_theme', 'acg_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function acg_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'acg' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'acg' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'acg_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function acg_scripts() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'Google-fonts', 'https://fonts.googleapis.com/css2?family=Lora:wght@400;700&family=Ubuntu:wght@300;400;700&display=swap',false );
    wp_enqueue_style( 'Google-lora', 'https://fonts.googleapis.com/css2?family=Lora:wght@400;700&display=swap',false );
	wp_enqueue_style( 'acg-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'acg-style', 'rtl', 'replace' );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.js');
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script( 'acg-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'jquery-slim', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js' );
    wp_enqueue_script( 'scoll-js', get_template_directory_uri() . '/js/jquery.paroller.js' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'acg_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
/**
 * Custom Menu
 */
function wpb_custom_new_menu() {
    register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

function wpb_custom_right_menu() {
    register_nav_menu('menu-right',__( 'Right Menu' ));
}
add_action( 'init', 'wpb_custom_right_menu' );

function wpb_custom_bottom_menu() {
    register_nav_menu('bottom_menu',__( 'Bottom Menu' ));
}
add_action( 'init', 'wpb_custom_bottom_menu' );
function register_my_menus() {
    register_nav_menus(
        array(
            'foils-footer-menu' => __( 'Foils-Menu' ),
            'Capsules-footer-menu' => __( 'Capsules-Menu' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

/**
 * Viewport Fix
 */
add_filter('ocean_meta_viewport', 'owp_child_viewport');
function owp_child_viewport( $viewport ){
    $viewport = '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">';
    return $viewport;
}

function custom_class( $classes ) {
    if (get_field('show_webinar') == 1 ) {
        $classes[] = 'show-webinar';
    }
    return $classes;
}
add_filter( 'body_class', 'custom_class' );


function on_submit( $form, &$abort, $submission )
{
    
    $data = $submission->get_posted_data();
    function callAPI($method, $url, $data, $auth)
    {
        $curl = curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $auth
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

    $paramdata = array(
        "grant_type" => "client_credentials",
        "client_id" => "j6c037g6mkgcn6utowi0prcd",
        "client_secret" => "Glk3N8Th5reQLttFUn5S8qnP",
        "account_id" => "110007442"
    );

    $get_data = callAPI('POST', 'https://mc6thw56ztljt4rkw-k3-q4rbmy0.auth.marketingcloudapis.com/v2/token', json_encode($paramdata), '');

    $response = json_decode($get_data, true);
	/*APIEvent-fab21c55-5c2d-7266-3e79-45048d11ec5e*/
    if(isset($response['errorcode']))
    {
        $ret = 0;
    }
    else{
        $access_token = $response['access_token'];
        {
            if ( $form->ID() == 6745 ) {
                $paramdata_apimail = array(
                    "ContactKey" => sanitize_text_field($data['cf7zwr-email']),
                    "EventDefinitionKey"=> "APIEvent-e9dc9480-ca6c-95f6-54b3-b9d10a5330ca",
                    "Data"=> array(
                        "First Name" => sanitize_text_field($data['cf7zwr-first_name']),
                        "Last Name" => sanitize_text_field($data['cf7zwr-last_name']),
                        "Full Name" => sanitize_text_field($data['cf7zwr-first_name']) + sanitize_text_field($data['cf7zwr-last_name']),
                        "Job Title" => sanitize_text_field($data['jobtitle']),
                        "Area of Responsibility" => sanitize_text_field($data['responsibility']),
                        "City" => sanitize_text_field($data['city']),
                        "Has your company previously purchased equipment or consumables from ACG in the past 2 years" => sanitize_text_field($data['question'][0]),
                        "Do you have plans to purchase granulation equipment" => sanitize_text_field($data['purchase'][0]),
                        "What would you like to learn at this event" => sanitize_text_field($data['your-message']),
                        "I consent to receiving emails" => "True",
                        "Email" => sanitize_text_field($data['cf7zwr-email']),
                        "Company Name" => sanitize_text_field($data['companyname']),
                        "Country" => sanitize_text_field($data['Country'][0]),
                        "Contact Number" => sanitize_text_field($data['contact']),
                        "Link" => sanitize_text_field("https://staging-acgworld.kinsta.cloud/2102-capsules-webinar"),
                        "Source" => sanitize_text_field($data['utm_source']) . '|' . sanitize_text_field($data['utm_pub'])
                    )
                );
            }
//
            $get_data_apimail = callAPI('POST', 'https://mc6thw56ztljt4rkw-k3-q4rbmy0.rest.marketingcloudapis.com/interaction/v1/events', json_encode($paramdata_apimail), $access_token);
            $responsemail = json_decode($get_data_apimail, true);
        }
    }

	$to = 'charl@touchfoundry.co.za';
    $subject = 'ACG Response';
    $message = 'ACG Response';
    $message .= "\n" . print_r($responsemail,true);
    wp_mail( $to, $subject, $message );
}

add_action( 'wpcf7_before_send_mail', 'on_submit', 10, 3 );

function my_custom_mime_types( $mimes ) {

// New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc'] = 'application/msword';

// Optional. Remove a mime type.
    unset( $mimes['exe'] );

    return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );
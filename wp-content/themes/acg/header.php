<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ACG
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <script type="text/javascript" src="https://www.bugherd.com/sidebarv2.js?apikey=ztsnrslqhnzx1azgz7cqiw" async="true"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site page-header">

    <nav class="navbar navbar-expand-lg navbar-dark" id="nav">
        <div class="container-fluid">

            <!-- Brand -->
            <a class="navbar-brand" href="http://www.acg-world.com/">
                <svg width="80" height="26" viewBox="0 0 80 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M23.3158 9.60363V15.2197H18.4249L17.4377 16.3537L14.5122 19.7197H23.3158V25.3041H28.8301V3.27148L23.3158 9.60363Z" fill="white"/>
                    <path d="M21.7051 0.244141L0 25.3039H7.12508L28.8301 0.244141H21.7051Z" fill="white"/>
                    <path d="M51.8748 17.7698C50.9034 18.6711 49.9252 19.3805 48.9425 19.9023C47.9598 20.4242 46.758 20.684 45.3348 20.684C44.2685 20.684 43.2904 20.4761 42.4025 20.0627C41.5147 19.6493 40.7557 19.0845 40.1277 18.3752C39.4996 17.6636 39.0072 16.8345 38.6525 15.888C38.2956 14.9414 38.1194 13.9339 38.1194 12.8676V12.7976C38.1194 11.7313 38.2978 10.7305 38.6525 9.79529C39.0072 8.86004 39.5064 8.03548 40.1457 7.32614C40.7851 6.61453 41.5486 6.0588 42.4387 5.65443C43.3265 5.25232 44.2934 5.05126 45.3348 5.05126C46.5908 5.05126 47.7204 5.29976 48.7279 5.79675C49.7355 6.29375 50.7249 6.9805 51.6963 7.85702L55.1798 3.8404C54.6105 3.27112 53.9961 2.75154 53.3319 2.27713C52.6677 1.80273 51.9471 1.40062 51.1632 1.06854C50.3815 0.734195 49.5118 0.474403 48.5517 0.284642C47.5916 0.0948806 46.5321 0 45.3709 0C43.453 0 41.6977 0.338859 40.1119 1.01206C38.5237 1.68752 37.1615 2.61147 36.0252 3.78393C34.8889 4.95638 33.9988 6.31859 33.3595 7.87057C32.7202 9.42254 32.3994 11.0875 32.3994 12.8631V12.9331C32.3994 14.7087 32.7202 16.3805 33.3595 17.9437C33.9988 19.507 34.8889 20.8647 36.0252 22.0123C37.1615 23.1622 38.5124 24.068 40.0757 24.7322C41.639 25.3941 43.3333 25.7262 45.1586 25.7262C46.3672 25.7262 47.4561 25.6245 48.4275 25.4235C49.3989 25.2224 50.2934 24.9265 51.1112 24.5357C51.929 24.1448 52.6813 23.6772 53.368 23.1328C53.978 22.6493 54.5676 22.1094 55.1391 21.5153L51.8816 17.7766L51.8748 17.7698Z" fill="white"/>
                    <path d="M68.4695 15.7073H74.7068V19.2856C73.3084 20.2886 71.5915 20.7879 69.5538 20.7879C68.4401 20.7879 67.4281 20.5913 66.5154 20.2005C65.6027 19.8074 64.8098 19.2562 64.1344 18.5446C63.4589 17.833 62.937 16.9949 62.5711 16.0348C62.2029 15.0725 62.0199 14.022 62.0199 12.8812V12.8111C62.0199 11.7426 62.2029 10.7396 62.5711 9.80207C62.937 8.86456 63.4476 8.04452 64.0982 7.34421C64.7488 6.6439 65.5079 6.08591 66.3731 5.67024C67.236 5.25458 68.1668 5.04674 69.163 5.04674C69.8724 5.04674 70.5252 5.10548 71.1171 5.22521C71.709 5.34494 72.2602 5.50985 72.7685 5.7222C73.2768 5.93455 73.7693 6.19661 74.2437 6.50384C74.6887 6.793 75.1337 7.12508 75.5788 7.49782L78.9809 3.56932L79.1119 3.41118C78.4726 2.86675 77.8085 2.38105 77.1217 1.95409C76.4349 1.52713 75.7007 1.17245 74.9191 0.887811C74.1375 0.603169 73.2903 0.38404 72.3777 0.230424C71.4673 0.0768081 70.4529 0 69.3415 0C67.4461 0 65.7044 0.3366 64.1163 1.01206C62.5282 1.68752 61.1546 2.61147 59.9935 3.78393C58.8323 4.95638 57.9265 6.31859 57.2736 7.87057C56.623 9.42254 56.2954 11.0875 56.2954 12.8631V12.9331C56.2954 14.781 56.6162 16.4866 57.2555 18.0499C57.8948 19.6132 58.7894 20.9641 59.9393 22.1004C61.0891 23.2367 62.4626 24.1245 64.0621 24.7661C65.6615 25.4054 67.4326 25.7262 69.3754 25.7262C71.6502 25.7262 73.6698 25.3399 75.4342 24.5718C77.1985 23.8015 78.7211 22.873 79.9997 21.7819V10.9813H72.5606L68.4695 15.7073Z" fill="white"/>
                </svg>
            </a>

            <!-- Toggler -->
            <?php
            if(is_front_page() || is_page(1520) || is_page(3052) || is_page(2167) || is_page(6771) || is_page(6775) || is_page(6775)) :?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <?php
            elseif(is_page(4192) || is_page(5365) || is_page(5480) || is_page(5537) || is_page(6559) || is_page(6731) || is_page(6775) || is_page(8242)) :?>
                <div class="navbar-toggler text-white">
                    <?php the_field( 'register_text', $post->ID  ); ?>
                </div>
            <?php
            endif;
            ?>


            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <?php
                if(is_front_page() || is_page(1520) || is_page(3052) || is_page(2167) || is_page(6771)) :?>
                <?php
                     wp_nav_menu( array(
                    'theme_location' => 'main-menu',
                    'container_class' => 'navbar-nav mr-auto' ) );
                ?>

                <!-- Button -->
                <div class="right-menu">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-right',
                        'container_class' => 'navbar-nav' ) );
                    ?>
                     </div>

                <?php elseif(is_page(9674) || is_page(9967) || is_page(9902) || is_page(9119)) :?>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'Russian-main-menu',
                        'container_class' => 'navbar-nav mr-auto' ) );
                    ?>

                    <!-- Button -->
                    <div class="right-menu">
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'Russian-main-menu-right',
                            'container_class' => 'navbar-nav' ) );
                        ?>
                    </div>
                    <?php

                    ?>
                <?php elseif(is_page(10184) || is_page(10133) || is_page(10163) || is_page(9450)) :?>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'Korea-main-menu',
                        'container_class' => 'navbar-nav mr-auto' ) );
                    ?>

                    <!-- Button -->
                    <div class="right-menu">
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'Korea-main-menu-right',
                            'container_class' => 'navbar-nav' ) );
                        ?>
                    </div>
                    <?php

                    ?>
                <?php elseif(is_page(9986) || is_page(10101) || is_page(9440) || is_page(10079)) :?>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'Brazil-main-menu',
                        'container_class' => 'navbar-nav mr-auto' ) );
                    ?>

                    <!-- Button -->
                    <div class="right-menu">
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'Brazil-main-menu-right',
                            'container_class' => 'navbar-nav' ) );
                        ?>
                    </div>
                    <?php

                    ?>
                <?php
                elseif(is_page(4192) || is_page(5365) || is_page(5480) || is_page(5537) || is_page(6559) || is_page(6731) || is_page(6775) || is_page(8242)) :?>
                    <div class="left-text mr-auto"><?php the_field( 'register_date', $post->ID ); ?></div>
                    <div class="right-menu">
                        <?php the_field( 'register_text', $post->ID  ); ?>
                    </div>
                    <?php
                endif;
                ?>
            </div>

        </div>
    </nav>


<?php
/*
 * Template Name: Campaign Page
 * description: >-
  Page template without sidebar
 */

get_header(); ?>

<div id="primary" class="site-content">
    <div id="content" role="main">
        <section class="banner">
            <div class="height-85vh image-container fade-in"
                 style="background-position: center;background-repeat: no-repeat; background-size: contain;">
                <div class="container space-2 space-0--lg mt-lg-8">
                    <div class="row justify-content-lg-between align-items-lg-center">
                    </div>
                </div>
            </div>
        </section>
        <section class="GTX-One py-6">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5 col-lg-6 order-md-2 order-2">
                        <div class="features mt-md-0 mt-5">
                            <?php
                            if (have_rows('info_section', $post->ID)):
                                while (have_rows('info_section' , $post->ID)) : the_row(); ?>
                                    <div class="d-flex mb-5">
                                        <!-- Badge -->
                                        <div class="badge">
                                            <img src="<?php the_sub_field('image', $post->ID); ?>" alt="" width="92"
                                                 height="92">
                                        </div>
                                        <div class="ml-2">
                                            <!-- Heading -->
                                            <h6 class="feature-header">
                                                <?php the_sub_field('header', $post->ID); ?>
                                            </h6>
                                            <!-- Text -->
                                            <p class="text-muted mb-6 mb-md-0">
                                                <?php the_sub_field('description', $post->ID); ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            else :
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-6 order-md-1 order-1">

                        <!-- Heading -->
                        <h1 class="mb-2">
                            <?php the_field('gtx-one_header', $post->ID); ?>
                        </h1>
                        <h4>
                            <?php the_field('sub_header_1', $post->ID); ?><br>
                            <?php the_field('sub_header_2', $post->ID); ?><br>
                            <span class="grey">
                                <?php the_field('sub_header_3', $post->ID); ?>
                            </span>
                        </h4>

                        <div class="links">
                            <?php
                            if (have_rows('links')):
                                while (have_rows('links')) : the_row(); ?>
                                    <?php if ( get_sub_field( 'icon' ) ) : ?>
                                        <button type="button" class="btn btn-primary gtx-button" data-toggle="modal" data-target="#exampleModal">
                                            <?php the_sub_field('text', $post->ID); ?> <span class="icon">
                                                    <?php the_sub_field('icon', $post->ID); ?>
                                                </span>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <!--Content-->
                                                <div class="modal-content">


                                                    <div class="video-header justify-content-right">


                                                    </div>

                                                    <!--Body-->
                                                    <div class="modal-body video-player mb-0 p-0" style="text-align: right; margin:auto">
                                                        <a href="#" onClick="(function(){
    stopVideo();
    return false;
})();return false;"><svg id="stopvideo" data-dismiss="modal" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin:10px;">
                                                                <circle cx="12" cy="12" r="11" stroke="#111111" stroke-width="2"></circle>
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.08017 7.73013C7.69208 8.11823 7.69207 8.74745 8.08017 9.13554L9.32794 10.3833C10.109 11.1644 10.109 12.4307 9.32795 13.2117L7.73464 14.8051C7.34654 15.1931 7.34654 15.8224 7.73464 16.2105C8.12273 16.5986 8.75195 16.5986 9.14005 16.2105L10.7334 14.6172C11.5144 13.8361 12.7807 13.8361 13.5618 14.6172L15.1822 16.2376C15.5703 16.6257 16.1995 16.6257 16.5876 16.2376C16.9757 15.8495 16.9757 15.2203 16.5876 14.8322L14.9672 13.2117C14.1861 12.4307 14.1861 11.1644 14.9672 10.3833L16.2421 9.10841C16.6302 8.72031 16.6302 8.09109 16.2421 7.703C15.854 7.3149 15.2248 7.3149 14.8367 7.703L13.5618 8.97791C12.7807 9.75896 11.5144 9.75896 10.7334 8.97791L9.48558 7.73013C9.09749 7.34204 8.46826 7.34204 8.08017 7.73013Z" fill="#111111"></path>
                                                            </svg></a>

                                                        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                                            <video id="product_video" class="video-js vjs-default-skin" controls="" preload="auto" width="1600" height="900" data-setup="{&quot;example_option&quot;:true}">
                                                                <source src="https://acg-webinar.s3.amazonaws.com/web_product_video.mp4" type="video/mp4">
                                                                <source src="https://acg-webinar.s3.amazonaws.com/web_product_video+copy.webm" type="video/webm">
                                                                <!--source src='../dist/video/product_video.webm' type=video/webm>
                                                                <source src='../dist/video/product_video.ogv' type=video/ogg>

                                                                <source src='../dist/video/product_video.3gp' type=video/3gp-->
                                                            </video>
                                                        </div>
                                                        <script type="text/javascript">
                                                            const video = document.querySelector("#product_video");
                                                            function stopVideo() {
                                                                video.pause();
                                                                video.currentTime = 0;
                                                            }
                                                            var vid = document.getElementById("product_video");
                                                            function playVid() { vid.play(); }
                                                        </script>
                                                    </div>

                                                </div>
                                                <!--/.Content-->
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="link">
                                            <a href="<?php the_sub_field('url', $post->ID); ?>"><?php the_sub_field('text', $post->ID); ?></a>
                                        </div>
                                    <?php endif ?>

                                <?php
                                endwhile;
                            else :
                            endif;
                            ?>
                        </div>
                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .container -->
        </section>
        <section class="xxx-contries pt-6-5 pb-md-12 pb-5">
            <img src="<?php the_field( 'countries_background',  $post->ID); ?>" alt="" class="img-parallax" data-speed="1">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6 order-md-1 order-1">
                        <h1 class="m-0">
                            <?php the_field('countries_header', $post->ID); ?>
                        </h1>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 order-md-2 order-2">
                        <div class="features mt-md-0">
                            <?php
                            if (have_rows('info_section_countries', $post->ID)):
                                while (have_rows('info_section_countries', $post->ID)) : the_row(); ?>
                                    <div class="d-flex mb-5 mt-3 mt-md-0">
                                        <div class="badge">
                                            <img src="<?php the_sub_field('image', $post->ID); ?>" alt="" width="92"
                                                 height="92">
                                        </div>
                                        <div class="ml-2">
                                            <h6 class="feature-header">
                                                <?php the_sub_field('header', $post->ID); ?>
                                            </h6>
                                            <p class="text-muted mb-6 mb-md-0">
                                                <?php the_sub_field('description', $post->ID); ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            else :
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
        </section>

        <section class="integrated-design">
            <div class="row justify-content-center py-11">
                <div class="col-12 col-md-10 col-lg-8 text-center py-5">
                    <h1>
                        <span class="heading-gray"><?php the_field( 'design_first_header', $post->ID ); ?></span><br>
                        <?php the_field( 'design_second_design', $post->ID ); ?><br>
                        <?php the_field( 'design_third_header', $post->ID ); ?><br>
                        <?php the_field( 'design_fourth_header', $post->ID ); ?>
                    </h1>
                </div>
            </div>
        </section>

        <section class="slider">

                <div class="row ml-0 mr-0">
                    <div class="col-12 col-md-9 order-md-1 pr-0 pl-0">
                        <!-- Silder -->
                        <?php
                        if (have_rows('slides', $post->ID)):
                            while (have_rows('slides', $post->ID)) : the_row(); ?>
                                <div class="mySlides">
                                    <img src="<?php the_sub_field('slide_image', $post->ID); ?>" alt="">
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                    <div class="col-12 col-md-3 order-md-2 detail">
                        <div class="dots py-4">
                            <div class="dots-container" id="dots-container">
                                <span class="dot" id="dot"></span>
                            </div>

                        </div>
                        <?php
                        if (have_rows('slides', $post->ID)):
                            while (have_rows('slides', $post->ID)) : the_row(); ?>

                                <div class="detail-div">
                                    <!-- Heading -->
                                    <h5>
                                        <?php the_sub_field('header', $post->ID); ?>
                                    </h5>

                                    <!-- Text -->
                                    <?php the_sub_field('description', $post->ID); ?>
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                        <div class="nav-arrow mb-3">
                            <div class="d-flex">
                                <div class="back mr-1" id="back-slid">
                                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="16" cy="16" r="15" stroke="currentColor" stroke-width="2"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M18.455 9.79715C18.9431 10.2853 18.9431 11.0768 18.455 11.5649L15.4495 14.5703C14.6685 15.3514 14.6685 16.6177 15.4495 17.3988L18.4552 20.4044C18.9433 20.8925 18.9433 21.684 18.4552 22.1722C17.967 22.6603 17.1756 22.6603 16.6874 22.1722L11.3841 16.8689C11.2191 16.7039 11.1099 16.5041 11.0564 16.2932C10.9515 15.8801 11.0607 15.4237 11.384 15.1004L16.6872 9.79715C17.1754 9.309 17.9668 9.309 18.455 9.79715Z"
                                              fill="currentColor"/>
                                    </svg>
                                </div>
                                <div class="forward" id="forward-slid">
                                    <svg width="33" height="32" viewBox="0 0 33 32" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="16.3335" cy="16" r="15" stroke="currentColor" stroke-width="2"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M14.0206 9.79715C13.5324 10.2853 13.5324 11.0768 14.0206 11.5649L17.026 14.5703C17.8071 15.3514 17.8071 16.6177 17.026 17.3988L14.0204 20.4044C13.5323 20.8925 13.5323 21.684 14.0204 22.1722C14.5086 22.6603 15.3 22.6603 15.7882 22.1722L21.0914 16.8689C21.2565 16.7039 21.3657 16.5041 21.4192 16.2932C21.5241 15.8801 21.4149 15.4237 21.0916 15.1004L15.7884 9.79715C15.3002 9.309 14.5087 9.309 14.0206 9.79715Z"
                                              fill="currentColor"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- / .row -->
            <script type="text/javascript">
                //    Slider Functionality

                let slideIndex = 1;
                const slides = document.getElementsByClassName("mySlides");

                const detail_divs = document.getElementsByClassName("detail-div");

                const dots = document.getElementsByClassName("dot");
                // for (let i = 1; i < slides.length; i++) {
                //     let dot = document.getElementById("dot");
                //     let copiedDot = dot.cloneNode(true);
                //     document.getElementById("dots-container").append(copiedDot);
                // }

                for (let i = 1; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }

                //Button Click Listeners
                //Forward Click
                let forwardButtonClick = document.getElementById("forward-slid").addEventListener("click", function () {
                    plusSlides(1);
                });
                //Back Click
                let backButtonClick = document.getElementById("back-slid").addEventListener("click", function () {
                    plusSlides(-1);
                });

                //Div References
                let forwardButton = document.getElementById("forward-slid");
                let backButton = document.getElementById("back-slid");
                showSlides(slideIndex);

                function plusSlides(n) {
                    showSlides(slideIndex += n, n);
                    checkButtons();
                }


                function checkButtons() {
                    if (slideIndex == slides.length) {
                        forwardButton.classList.add(
                            'disable'
                        )
                    } else {
                        forwardButton.classList.remove('disable');
                    }
                    if (slideIndex === 1) {
                        backButton.classList.add('disable');
                    } else {
                        backButton.classList.remove('disable');
                    }
                }


                function currentSlide(n) {
                    showSlides(slideIndex = n);
                }

                function showSlides(n, b) {
                    let i;
                    if (n > slides.length) {
                        slideIndex = 1
                    }

                    if (n < 1) {
                        slideIndex = slides.length;
                    }

                    for (i = 0; i < slides.length; i++) {
                        slides[i].style.display = "none";
                    }

                    for (i = 0; i < detail_divs.length; i++) {
                        detail_divs[i].style.display = "none";
                    }

                    for (i = 0; i < dots.length; i++) {
                        dots[i].className = dots[i].className.replace(" active", "");
                    }

                    slides[slideIndex - 1].style.display = "block";
                    if (b === 1) {
                        slides[slideIndex - 1].classList.add('fade-in-right');
                    } else {
                        slides[slideIndex - 1].classList.remove('fade-in-right');
                    }
                    if (b === -1) {
                        slides[slideIndex - 1].classList.add('fade-in-left');
                    } else {
                        slides[slideIndex - 1].classList.remove('fade-in-left');
                    }
                    detail_divs[slideIndex - 1].style.display = "block";
                    dots[slideIndex - 1].className += " active";
                    checkButtons();
                }
            </script>
        </section>
        <section class="mixer-one pt-11">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8 text-center m-auto pb-3">
                        <h4>
                            <?php the_field( 'mixer_xone_header', $post->ID ); ?>
                        </h4>
                    </div>
                </div>
                <div class="row px-5">
                    <div class="col-12 col-md-3 mt-6">
                        <?php
                        if (have_rows('left_features_mixer_xone', $post->ID)):
                            while (have_rows('left_features_mixer_xone', $post->ID)) : the_row(); ?>
                                <div class="entry">
                                    <!-- Heading -->
                                    <h6>
                                        <?php the_sub_field('header', $post->ID); ?>
                                    </h6>
                                    <!-- Text -->
                                    <p class="text-muted mb-6 mb-md-0 pb-4">
                                        <?php the_sub_field('description', $post->ID); ?>
                                    </p>
                                    <?php if ( get_sub_field( 'bottom_line' ) == 1 ) : ?>
                                        <hr>
                                    <?php else : ?>

                                    <?php endif; ?>
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                    <div class="col-12 col-md-6 mb-5 mb-md-0 text-center">
                        <img src="<?php the_field( 'mixer_xone_feature_image', $post->ID); ?>" alt="" style="width: 630px">
                    </div>
                    <div class="col-12 col-md-3 mt-6">
                        <?php
                        if (have_rows('right_features_mixer_xone', $post->ID)):
                            while (have_rows('right_features_mixer_xone', $post->ID)) : the_row(); ?>
                                <div class="entry">
                                    <!-- Heading -->
                                    <h6>
                                        <?php the_sub_field('header', $post->ID); ?>
                                    </h6>
                                    <!-- Text -->
                                    <p class="text-muted mb-6 mb-md-0 pb-4">
                                        <?php the_sub_field('description', $post->ID); ?>
                                    </p>
                                    <?php if ( get_sub_field( 'bottom_line' ) == 1 ) : ?>
                                        <hr>
                                    <?php else : ?>

                                    <?php endif; ?>
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div> <!-- / .row -->
            </div>
        </section>


        <section class="stats pt-13 pt-md-13">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-8 first col-md-3 text-left mx-4">
                        <h1>
                            <?php the_field( 'mixer_xone_colomn_one_header', $post->ID ); ?>
                        </h1>
                        <hr>
                        <p class="text-muted mb-6 mb-md-0 text-left">
                            <?php the_field( 'mixer_xone_colomn_one_description', $post->ID ); ?>
                        </p>
                    </div>
                    <div class="col-8 second col-md-3 text-left mx-4">
                        <h1>
                            <?php the_field( 'mixer_xone_colomn_two_header', $post->ID ); ?>
                        </h1>
                        <hr>
                        <p class="text-muted mb-6 mb-md-0 text-left">
                            <?php the_field( 'mixer_xone_colomn_two_description', $post->ID ); ?>
                        </p>
                    </div>
                    <div class="col-8 third col-md-3 text-left mx-4">
                        <h1>
                            <?php the_field( 'mixer_xone_colomn_three_header', $post->ID ); ?>
                        </h1>
                        <hr>
                        <p class="text-muted mb-0 text-left">
                            <?php the_field( 'mixer_xone_colomn_three_description', $post->ID ); ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="mixer-one pt-11 pb-11">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8 text-center m-auto pb-3">
                        <h4><?php the_field( 'shear_mixer_header', $post->ID ); ?>
                        </h4>
                    </div>
                </div>
                <div class="row px-5">
                    <div class="col-12 col-md-3 mt-6">
                        <?php
                        if (have_rows('shear_mixer_left_features', $post->ID)):
                            while (have_rows('shear_mixer_left_features', $post->ID)) : the_row(); ?>
                                <div class="entry">
                                    <!-- Heading -->
                                    <h6>
                                        <?php the_sub_field('header', $post->ID); ?>
                                    </h6>
                                    <!-- Text -->
                                    <p class="text-muted mb-6 mb-md-0 pb-4">
                                        <?php the_sub_field('description', $post->ID); ?>
                                    </p>
                                    <?php if ( get_sub_field( 'bottom_line' ) == 1 ) : ?>
                                        <hr>
                                    <?php else : ?>

                                    <?php endif; ?>
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                    <div class="col-12 col-md-6 mb-5 mb-md-0 text-center">
                        <img src="<?php the_field( 'shear_mixer_feature_image' ); ?>" alt="" style="width: 459px;">
                    </div>
                    <div class="col-12 col-md-3 mt-6">
                        <?php
                        if (have_rows('shear_mixer_right_features', $post->ID)):
                            while (have_rows('shear_mixer_right_features', $post->ID)) : the_row(); ?>
                                <div class="entry">
                                    <!-- Heading -->
                                    <h6>
                                        <?php the_sub_field('header', $post->ID); ?>
                                    </h6>
                                    <!-- Text -->
                                    <p class="text-muted mb-6 mb-md-0 pb-4">
                                        <?php the_sub_field('description', $post->ID); ?>
                                    </p>
                                    <?php if ( get_sub_field( 'bottom_line' ) == 1 ) : ?>
                                        <hr>
                                    <?php else : ?>

                                    <?php endif; ?>
                                </div>
                            <?php
                            endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div> <!-- / .row -->
            </div>
        </section>


        <section class="testimonials"
                 style="background-image: url('<?php the_field( 'testimonial_1_background', $post->ID); ?>'); background-size: cover; background-position: top;background-repeat: no-repeat">
            <div class="container-fluid pt-12">
                <div class="row justify-content-end">
                    <div class="col-12 col-md-3 testimonial pt-3 pb-2 pl-5 pr-5">
                        <div class="icon">
                            <svg width="20" height="15" viewBox="0 0 20 15" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.315125 14.24C0.251125 13.792 0.203125 13.328 0.171125 12.848C0.139125 12.368 0.123125 12.016 0.123125 11.792C0.123125 9.648 0.507125 7.616 1.27513 5.696C2.04313 3.744 2.98713 2.096 4.10713 0.751999L8.95513 1.856C8.34713 3.232 7.85113 4.8 7.46713 6.56C7.11513 8.288 6.93913 9.936 6.93913 11.504C6.93913 11.568 6.93913 11.712 6.93913 11.936C6.93913 12.128 6.93913 12.368 6.93913 12.656C6.97113 12.912 6.98713 13.184 6.98713 13.472C7.01913 13.76 7.05113 14.016 7.08313 14.24H0.315125ZM10.6351 14.24C10.5711 13.792 10.5231 13.328 10.4911 12.848C10.4591 12.368 10.4431 12.016 10.4431 11.792C10.4431 9.648 10.8271 7.616 11.5951 5.696C12.3631 3.744 13.3071 2.096 14.4271 0.751999L19.2751 1.856C18.6671 3.232 18.1711 4.8 17.7871 6.56C17.4351 8.288 17.2591 9.936 17.2591 11.504C17.2591 11.568 17.2591 11.712 17.2591 11.936C17.2591 12.128 17.2591 12.368 17.2591 12.656C17.2911 12.912 17.3071 13.184 17.3071 13.472C17.3391 13.76 17.3711 14.016 17.4031 14.24H10.6351Z"
                                      fill="#111111"/>
                            </svg>
                        </div>
                        <div class="comment">
                            <?php the_field( 'testimonial_1_description', $post->ID ); ?>
                        </div>
                        <hr>
                        <div class="author"><?php the_field( 'testimonial_1_author', $post->ID ); ?></div>
                        <div class="text"><?php the_field( 'testimonial_1_author_role', $post->ID ); ?></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="xxx-contries pt-6-5 pb-md-12 pb-5">
            <img src="<?php the_field( 'aisa_background', $post->ID ); ?>" alt="" class="img-parallax" data-speed="1">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6 order-md-1 order-1">
                        <h1 class="m-0">
                            <?php the_field( 'aisa_header', $post->ID ); ?>
                        </h1>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 order-md-2 order-2">
                        <div class="features mt-md-0">
                            <?php
                            if (have_rows('shear_mixer_right_features', $post->ID)):
                                while (have_rows('aisa_features', $post->ID)) : the_row(); ?>
                                    <div class="d-flex mb-5 mt-3 mt-md-0">
                                        <div class="badge">
                                            <img src="<?php the_sub_field('image', $post->ID); ?>" alt="" width="92"
                                                 height="92">
                                        </div>
                                        <div class="ml-2">
                                            <h6 class="feature-header">
                                                <?php the_sub_field('header', $post->ID); ?>
                                            </h6>
                                            <p class="text-muted mb-6 mb-md-0">
                                                <?php the_sub_field('description', $post->ID); ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            else :
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
        </section>


        <section class="integrated-design py-8">
            <div class="row justify-content-center py-5">
                <div class="col-12 col-md-10 col-lg-8 text-center py-5">
                    <h1>
                        <span class="heading-gray"><?php the_field( 'wall_design_header_1', $post->ID ); ?></span>
                        <?php the_field( 'wall_design_header_2', $post->ID ); ?>
                        <?php the_field( 'wall_design_header_3', $post->ID ); ?>
                        <?php the_field( 'wall_design_header_4', $post->ID ); ?>
                    </h1>
                </div>
            </div>
        </section>


        <section class="testimonials second"
                 style="background-image: url('<?php the_field( 'testimonial_2_background', $post->ID ); ?>'); background-size: cover; background-position: top;background-repeat: no-repeat">
            <div class="container-fluid pt-12">
                <div class="row justify-content-end">
                    <div class="col-12 col-md-3 testimonial pt-3 pb-2 pl-5 pr-5">
                        <div class="icon">
                            <svg width="20" height="15" viewBox="0 0 20 15" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.315125 14.24C0.251125 13.792 0.203125 13.328 0.171125 12.848C0.139125 12.368 0.123125 12.016 0.123125 11.792C0.123125 9.648 0.507125 7.616 1.27513 5.696C2.04313 3.744 2.98713 2.096 4.10713 0.751999L8.95513 1.856C8.34713 3.232 7.85113 4.8 7.46713 6.56C7.11513 8.288 6.93913 9.936 6.93913 11.504C6.93913 11.568 6.93913 11.712 6.93913 11.936C6.93913 12.128 6.93913 12.368 6.93913 12.656C6.97113 12.912 6.98713 13.184 6.98713 13.472C7.01913 13.76 7.05113 14.016 7.08313 14.24H0.315125ZM10.6351 14.24C10.5711 13.792 10.5231 13.328 10.4911 12.848C10.4591 12.368 10.4431 12.016 10.4431 11.792C10.4431 9.648 10.8271 7.616 11.5951 5.696C12.3631 3.744 13.3071 2.096 14.4271 0.751999L19.2751 1.856C18.6671 3.232 18.1711 4.8 17.7871 6.56C17.4351 8.288 17.2591 9.936 17.2591 11.504C17.2591 11.568 17.2591 11.712 17.2591 11.936C17.2591 12.128 17.2591 12.368 17.2591 12.656C17.2911 12.912 17.3071 13.184 17.3071 13.472C17.3391 13.76 17.3711 14.016 17.4031 14.24H10.6351Z"
                                      fill="#fff"/>
                            </svg>
                        </div>
                        <div class="comment">
                            <?php the_field( 'testimonial_2_description',$post->ID ); ?>
                        </div>
                        <hr>
                        <div class="author"><?php the_field( 'testimonial_2_author',$post->ID ); ?></div>
                        <div class="text"><?php the_field( 'testimonial_2_role',$post->ID ); ?></div>
                    </div>
                </div>
            </div>
        </section>


        <section class="contact py-8">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6 order-2">
                        <div class="form-container">
                            <?php
                            $shortcode = get_field( 'contact_shortcode' );
                            echo do_shortcode($shortcode);
                            ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 order-1">
                        <h2>
                            <?php the_field( 'contact_header' ); ?>
                        </h2>
                        <?php the_field( 'contact_description' ); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<?php get_footer(); ?>

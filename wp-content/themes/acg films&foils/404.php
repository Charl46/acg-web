<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ACG
 */

get_header();
?>

    <main id="primary" class="site-main">

        <section class="error-404 not-found">
            <div class="container d-flex flex-column">
                <div class="row align-items-center justify-content-center no-gutters min-vh-100">
                    <div class="col-12 col-md-5 col-lg-4 py-8 py-md-11">

                        <!-- Heading -->
                        <h1 class="display-3 font-weight-bold text-center">
                            Error 404
                        </h1>

                        <!-- Text -->
                        <p class="mb-5 text-center text-muted">
                            Sorry, an error has occured, Requested page not found!
                        </p>

                        <!-- Link -->
                        <div class="text-center">
                            <a class="btn btn-primary" href="/">
                                Take Me Home
                            </a>
                        </div>

                    </div>
                </div> <!-- / .row -->
            </div>
        </section><!-- .error-404 -->

    </main><!-- #main -->

<?php
get_footer();

$(document).ready(function () {
    var element = document.getElementById("nav");

    window.onscroll = function () {
        checkHeader()
    };

    function checkHeader() {
        if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
            element.classList.add("active");
        } else {
            element.classList.remove('active');
        }
    }


//    Slider Functionality

    let slideIndex = 1;
    const slides = document.getElementsByClassName("mySlides");

    const detail_divs = document.getElementsByClassName("detail-div");

    const dots = document.getElementsByClassName("dot");

    //Dots Configuration
    for (let i = 1; i < slides.length; i++) {
        let dot = document.getElementById("dot");
        let copiedDot = dot.cloneNode(true);
        document.getElementById("dots-container").append(copiedDot);
    }

    for (let i = 1; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }

    //Button Click Listeners
    //Forward Click
    let forwardButtonClick = document.getElementById("forward-slid").addEventListener("click", function () {
        plusSlides(1);
    });
    //Back Click
    let backButtonClick = document.getElementById("back-slid").addEventListener("click", function () {
        plusSlides(-1);
    });

    //Div References
    let forwardButton = document.getElementById("forward-slid");
    let backButton = document.getElementById("back-slid");
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n, n);
        checkButtons();
    }


    function checkButtons() {
        if (slideIndex == slides.length) {
            forwardButton.classList.add(
                'disable'
            )
        } else {
            forwardButton.classList.remove('disable');
        }
        if (slideIndex === 1) {
            backButton.classList.add('disable');
        } else {
            backButton.classList.remove('disable');
        }
    }


    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n, b) {
        let i;
        if (n > slides.length) {
            slideIndex = 1
        }

        if (n < 1) {
            slideIndex = slides.length;
        }

        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }

        for (i = 0; i < detail_divs.length; i++) {
            detail_divs[i].style.display = "none";
        }

        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }

        slides[slideIndex - 1].style.display = "block";
        if (b === 1) {
            slides[slideIndex - 1].classList.add('fade-in-right');
        } else {
            slides[slideIndex - 1].classList.remove('fade-in-right');
        }
        if (b === -1) {
            slides[slideIndex - 1].classList.add('fade-in-left');
        } else {
            slides[slideIndex - 1].classList.remove('fade-in-left');
        }
        detail_divs[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        checkButtons();
    }

    $('.img-parallax').each(function () {
        var img = $(this);
        var imgParent = $(this).parent();

        function parallaxImg() {
            var speed = img.data('speed');
            var imgY = imgParent.offset().top;
            var winY = $(this).scrollTop();
            var winH = $(this).height();
            var parentH = imgParent.innerHeight();


            // The next pixel to show on screen
            var winBottom = winY + winH;

            // If block is shown on screen
            if (winBottom > imgY && winY < imgY + parentH) {
                // Number of pixels shown after block appear
                var imgBottom = ((winBottom - imgY) * speed);
                // Max number of pixels until block disappear
                var imgTop = winH + parentH;
                // Porcentage between start showing until disappearing
                var imgPercent = ((imgBottom / imgTop) * 100) + (50 - (speed * 50));
            }
            img.css({
                top: imgPercent + '%',
                transform: 'translate(-50%, -' + imgPercent + '%)'
            });
        }

        $(document).on({
            scroll: function () {
                parallaxImg();
            }, ready: function () {
                parallaxImg();
            }
        });
    });
    (function ($) {
        $.fn.visible = function (partial) {

            var $t = $(this),
                $w = $(window),
                viewTop = $w.scrollTop(),
                viewBottom = viewTop + $w.height(),
                _top = $t.offset().top,
                _bottom = _top + $t.height(),
                compareTop = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

        };

    })(jQuery);

    var win = $(window);

    var allMods = $("h1, h2, h3, h4");

    allMods.each(function (i, el) {
        var el = $(el);
        if (el.visible(true)) {
            el.addClass("already-visible");
        }
    });

    win.scroll(function (event) {

        allMods.each(function (i, el) {
            var el = $(el);
            if (el.visible(true)) {
                el.addClass("fade-in");
            }
        });
    });
    $(document).ready(function() {
        $("body").children().each(function() {
            $(this).html($(this).html().replace(/&#8232;/g," "));
        });
    });
    //Banner Steps
    // const bannerImages = {
    //    1: 'https://ablemana.sirv.com/Images/1.webp',
    //     2: 'https://ablemana.sirv.com/Images/2.webp',
    //     3: 'https://ablemana.sirv.com/Images/3.webp',
    //     4: 'https://ablemana.sirv.com/Images/4.webp',
    //     5: 'https://ablemana.sirv.com/Images/5.webp',
    //     6: 'https://ablemana.sirv.com/Images/6.webp',
    //     7: 'https://ablemana.sirv.com/Images/7.webp',
    //     8: 'https://ablemana.sirv.com/Images/8.webp',
    //     9: 'https://ablemana.sirv.com/Images/9.webp',
    //     10: 'https://ablemana.sirv.com/Images/10.webp',
    //     11: 'https://ablemana.sirv.com/Images/11.webp',
    //     12: 'https://ablemana.sirv.com/Images/12.webp',
    //     13: 'https://ablemana.sirv.com/Images/13.webp',
    //     14: 'https://ablemana.sirv.com/Images/14.webp',
    //     15: 'https://ablemana.sirv.com/Images/15.webp',
    //     16: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0016.webp',
    //     17: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0017.webp',
    //     18: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0018.webp',
    //     19: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0019.webp',
    //     20: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0020.webp',
    //     21: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0021.webp',
    //     22: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0022.webp',
    //     23: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0023.webp',
    //     24: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0024.webp',
    //     25: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0025.webp',
    //     26: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0026.webp',
    //     27: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0027.webp',
    //     28: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0028.webp',
    //     29: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0029.webp',
    //     30: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0030.webp',
    //     31: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0031.webp',
    //     32: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0032.webp',
    //     33: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0033.webp',
    //     34: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0034.webp',
    //     35: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0035.webp',
    //     36: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0036.webp',
    //     37: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0037.webp',
    //     38: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0038.webp',
    //     39: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0039.webp',
    //     40: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0040.webp',
    //     41: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0041.webp',
    //     42: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0042.webp',
    //     43: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0043.webp',
    //     44: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0044.webp',
    //     45: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0045.webp',
    //     46: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0046.webp',
    //     47: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0047.webp',
    //     48: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0048.webp',
    //     49: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0049.webp',
    //     50: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0050.webp',
    //     51: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0051.webp',
    //     52: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0052.webp',
    //     53: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0053.webp',
    //     54: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0054.webp',
    //     55: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0055.webp',
    //     56: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0056.webp',
    //     57: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0057.webp',
    //     58: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0058.webp',
    //     59: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0059.webp',
    //     60: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0060.webp',
    //     61: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0061.webp',
    //     62: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0062.webp',
    //     63: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0063.webp',
    //     64: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0064.webp',
    //     65: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0065.webp',
    //     66: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0066.webp',
    //     67: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0067.webp',
    //     68: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0068.webp',
    //     69: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0069.webp',
    //     70: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0070.webp',
    //     71: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0071.webp',
    //     72: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0072.webp',
    //     73: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0073.webp',
    //     74: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0074.webp',
    //     75: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0075.webp',
    //     76: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0076.webp',
    //     77: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0077.webp',
    //     78: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0078.webp',
    //     79: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0079.webp',
    //     80: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0080.webp',
    //     81: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0081.webp',
    //     82: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0082.webp',
    //     83: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0083.webp',
    //     84: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0084.webp',
    //     85: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0085.webp',
    //     86: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0086.webp',
    //     87: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0087.webp',
    //     88: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0088.webp',
    //     89: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0089.webp',
    //     90: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0090.webp',
    //     91: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0091.webp',
    //     92: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0092.webp',
    //     93: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0093.webp',
    //     94: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0094.webp',
    //     95: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0095.webp',
    //     96: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0096.webp',
    //     97: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0097.webp',
    //     98: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0098.webp',
    //     99: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0099.webp',
    //     100: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0100.webp',
    //     101: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0101.webp',
    //     102: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0102.webp',
    //     103: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0103.webp',
    //     104: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0104.webp',
    //     105: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0105.webp',
    //     106: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0106.webp',
    //     107: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0107.webp',
    //     108: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0108.webp',
    //     109: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0109.webp',
    //     110: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0110.webp',
    //     111: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0111.webp',
    //     112: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0112.webp',
    //     113: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0113.webp',
    //     114: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0114.webp',
    //     115: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0115.webp',
    //     116: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0116.webp',
    //     117: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0117.webp',
    //     118: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0118.webp',
    //     119: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0119.webp',
    //     120: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0120.webp',
    //     121: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0121.webp',
    //     122: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0122.webp',
    //     123: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0123.webp',
    //     124: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0124.webp',
    //     125: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0125.webp',
    //     126: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0126.webp',
    //     127: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0127.webp',
    //     128: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0128.webp',
    //     129: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0129.webp',
    //     130: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0130.webp',
    //     131: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0131.webp',
    //     132: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0132.webp',
    //     133: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0133.webp',
    //     134: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0134.webp',
    //     135: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0135.webp',
    //     136: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0136.webp',
    //     137: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0137.webp',
    //     138: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0138.webp',
    //     139: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0139.webp',
    //     140: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0140.webp',
    //     141: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0141.webp',
    //     142: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0142.webp',
    //     143: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0143.webp',
    //     144: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0144.webp',
    //     145: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0145.webp',
    //     146: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0146.webp',
    //     147: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0147.webp',
    //     148: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0148.webp',
    //     149: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0149.webp',
    //     150: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0150.webp',
    //     151: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0151.webp',
    //     152: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0152.webp',
    //     153: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0153.webp',
    //     154: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0154.webp',
    //     155: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0155.webp',
    //     156: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0156.webp',
    //     157: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0157.webp',
    //     158: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0158.webp',
    //     159: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0159.webp',
    //     160: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0160.webp',
    //     161: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0161.webp',
    //     162: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0162.webp',
    //     163: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0163.webp',
    //     164: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0164.webp',
    //     165: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0165.webp',
    //     166: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0166.webp',
    //     167: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0167.webp',
    //     168: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0168.webp',
    //     169: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0169.webp',
    //     170: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0170.webp',
    //     171: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0171.webp',
    //     172: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0172.webp',
    //     173: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0173.webp',
    //     174: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0174.webp',
    //     175: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0175.webp',
    //     176: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0176.webp',
    //     177: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0177.webp',
    //     178: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0178.webp',
    //     179: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0179.webp',
    //     180: 'https://ablemana.sirv.com/Images/GTXRotateWeb_0180.webp',
    // }
    // const step = 2;
    // let lastImage = false;
    // let hasScrolled = false;
    // function preloadNextImage(url) {
    //     let preloadLink = $('<link rel="preload" as="image" class="preload">').attr('href', url);
    //     $('footer').append(preloadLink);
    // }
    //
    // for (const [key, value] of Object.entries(bannerImages)) {
    //     preloadNextImage(value);
    // }
    // function trackScollPosition() {
    //
    //         const y = window.scrollY;
    //         const label = Math.min(Math.floor(y / step) + 1, 180);
    //         const imageToUse = bannerImages[label];
    //         //    Change the background image
    //         const imageContainer = $('.image-container');
    //         imageContainer.addClass('fade-in');
    //         imageContainer.css('background-image', `url(${imageToUse})`);
    //         if(label <= 179) {
    //             $('div.height-85vh.image-container.fade-in').addClass("intro");
    //             $('section.GTX-One.py-6').addClass("second-fixed");
    //         } else {
    //             $('div.height-85vh.image-container.fade-in').removeClass("intro");
    //             $('section.GTX-One.py-6').removeClass("second-fixed");
    //             if(lastImage === true){
    //                 lastImage = false;
    //             } else {
    //                 lastImage = true;
    //             }
    //         }
    // }
    //
    // function trackScollPositionUp() {
    //     const y = window.scrollY;
    //     const label = Math.min(Math.floor(y / step) + 1, 180);
    //     const imageToUse = bannerImages[label];
    //     const imageContainer = $('.image-container');
    //     imageContainer.addClass('fade-in');
    //     imageContainer.css('background-image', `url(${imageToUse})`);
    // }
    //
    // var lastScrollTop = 0;
    // $(window).scroll(function(event){
    //     var st = $(this).scrollTop();
    //     if (st > lastScrollTop){
    //         if (lastImage === false) {
    //             trackScollPosition();
    //         } else if(lastImage === true && hasScrolled === false) {
    //             hasScrolled = true
    //         }
    //     }
    //     else if(st == lastScrollTop)
    //     {
    //         //do nothing
    //         //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
    //     }
    //     else {
    //         if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
    //             // console.log('Up');
    //            if(lastImage === false) {
    //                trackScollPosition();
    //            } else {
    //                trackScollPositionUp();
    //            }
    //         } else {
    //             lastImage = false;
    //             // console.log('Third');
    //             trackScollPosition();
    //         }
    //     }
    //     lastScrollTop = st;
    // });

});
